export default class Player {
  constructor(name) {
    this.name = name;
    this.points = 0;
  }

  getName() {
    return this.name;
  }

  getPoints() {
    return this.points;
  }

  addPoints() {
    ++this.points;
  }
}
